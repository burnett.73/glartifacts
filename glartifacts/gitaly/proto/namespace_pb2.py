# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: namespace.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from . import lint_pb2 as lint__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='namespace.proto',
  package='gitaly',
  syntax='proto3',
  serialized_pb=_b('\n\x0fnamespace.proto\x12\x06gitaly\x1a\nlint.proto\"?\n\x13\x41\x64\x64NamespaceRequest\x12\x1a\n\x0cstorage_name\x18\x01 \x01(\tB\x04\x88\xc6,\x01\x12\x0c\n\x04name\x18\x02 \x01(\t\"B\n\x16RemoveNamespaceRequest\x12\x1a\n\x0cstorage_name\x18\x01 \x01(\tB\x04\x88\xc6,\x01\x12\x0c\n\x04name\x18\x02 \x01(\t\"N\n\x16RenameNamespaceRequest\x12\x1a\n\x0cstorage_name\x18\x01 \x01(\tB\x04\x88\xc6,\x01\x12\x0c\n\x04\x66rom\x18\x02 \x01(\t\x12\n\n\x02to\x18\x03 \x01(\t\"B\n\x16NamespaceExistsRequest\x12\x1a\n\x0cstorage_name\x18\x01 \x01(\tB\x04\x88\xc6,\x01\x12\x0c\n\x04name\x18\x02 \x01(\t\")\n\x17NamespaceExistsResponse\x12\x0e\n\x06\x65xists\x18\x01 \x01(\x08\"\x16\n\x14\x41\x64\x64NamespaceResponse\"\x19\n\x17RemoveNamespaceResponse\"\x19\n\x17RenameNamespaceResponse2\x81\x03\n\x10NamespaceService\x12S\n\x0c\x41\x64\x64Namespace\x12\x1b.gitaly.AddNamespaceRequest\x1a\x1c.gitaly.AddNamespaceResponse\"\x08\xfa\x97(\x04\x08\x01\x10\x02\x12\\\n\x0fRemoveNamespace\x12\x1e.gitaly.RemoveNamespaceRequest\x1a\x1f.gitaly.RemoveNamespaceResponse\"\x08\xfa\x97(\x04\x08\x01\x10\x02\x12\\\n\x0fRenameNamespace\x12\x1e.gitaly.RenameNamespaceRequest\x1a\x1f.gitaly.RenameNamespaceResponse\"\x08\xfa\x97(\x04\x08\x01\x10\x02\x12\\\n\x0fNamespaceExists\x12\x1e.gitaly.NamespaceExistsRequest\x1a\x1f.gitaly.NamespaceExistsResponse\"\x08\xfa\x97(\x04\x08\x02\x10\x02\x42\x34Z2gitlab.com/gitlab-org/gitaly/v15/proto/go/gitalypbb\x06proto3')
  ,
  dependencies=[lint__pb2.DESCRIPTOR,])




_ADDNAMESPACEREQUEST = _descriptor.Descriptor(
  name='AddNamespaceRequest',
  full_name='gitaly.AddNamespaceRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='storage_name', full_name='gitaly.AddNamespaceRequest.storage_name', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=_descriptor._ParseOptions(descriptor_pb2.FieldOptions(), _b('\210\306,\001')), file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='name', full_name='gitaly.AddNamespaceRequest.name', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=39,
  serialized_end=102,
)


_REMOVENAMESPACEREQUEST = _descriptor.Descriptor(
  name='RemoveNamespaceRequest',
  full_name='gitaly.RemoveNamespaceRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='storage_name', full_name='gitaly.RemoveNamespaceRequest.storage_name', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=_descriptor._ParseOptions(descriptor_pb2.FieldOptions(), _b('\210\306,\001')), file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='name', full_name='gitaly.RemoveNamespaceRequest.name', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=104,
  serialized_end=170,
)


_RENAMENAMESPACEREQUEST = _descriptor.Descriptor(
  name='RenameNamespaceRequest',
  full_name='gitaly.RenameNamespaceRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='storage_name', full_name='gitaly.RenameNamespaceRequest.storage_name', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=_descriptor._ParseOptions(descriptor_pb2.FieldOptions(), _b('\210\306,\001')), file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='from', full_name='gitaly.RenameNamespaceRequest.from', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='to', full_name='gitaly.RenameNamespaceRequest.to', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=172,
  serialized_end=250,
)


_NAMESPACEEXISTSREQUEST = _descriptor.Descriptor(
  name='NamespaceExistsRequest',
  full_name='gitaly.NamespaceExistsRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='storage_name', full_name='gitaly.NamespaceExistsRequest.storage_name', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=_descriptor._ParseOptions(descriptor_pb2.FieldOptions(), _b('\210\306,\001')), file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='name', full_name='gitaly.NamespaceExistsRequest.name', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=252,
  serialized_end=318,
)


_NAMESPACEEXISTSRESPONSE = _descriptor.Descriptor(
  name='NamespaceExistsResponse',
  full_name='gitaly.NamespaceExistsResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='exists', full_name='gitaly.NamespaceExistsResponse.exists', index=0,
      number=1, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=320,
  serialized_end=361,
)


_ADDNAMESPACERESPONSE = _descriptor.Descriptor(
  name='AddNamespaceResponse',
  full_name='gitaly.AddNamespaceResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=363,
  serialized_end=385,
)


_REMOVENAMESPACERESPONSE = _descriptor.Descriptor(
  name='RemoveNamespaceResponse',
  full_name='gitaly.RemoveNamespaceResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=387,
  serialized_end=412,
)


_RENAMENAMESPACERESPONSE = _descriptor.Descriptor(
  name='RenameNamespaceResponse',
  full_name='gitaly.RenameNamespaceResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=414,
  serialized_end=439,
)

DESCRIPTOR.message_types_by_name['AddNamespaceRequest'] = _ADDNAMESPACEREQUEST
DESCRIPTOR.message_types_by_name['RemoveNamespaceRequest'] = _REMOVENAMESPACEREQUEST
DESCRIPTOR.message_types_by_name['RenameNamespaceRequest'] = _RENAMENAMESPACEREQUEST
DESCRIPTOR.message_types_by_name['NamespaceExistsRequest'] = _NAMESPACEEXISTSREQUEST
DESCRIPTOR.message_types_by_name['NamespaceExistsResponse'] = _NAMESPACEEXISTSRESPONSE
DESCRIPTOR.message_types_by_name['AddNamespaceResponse'] = _ADDNAMESPACERESPONSE
DESCRIPTOR.message_types_by_name['RemoveNamespaceResponse'] = _REMOVENAMESPACERESPONSE
DESCRIPTOR.message_types_by_name['RenameNamespaceResponse'] = _RENAMENAMESPACERESPONSE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

AddNamespaceRequest = _reflection.GeneratedProtocolMessageType('AddNamespaceRequest', (_message.Message,), dict(
  DESCRIPTOR = _ADDNAMESPACEREQUEST,
  __module__ = 'namespace_pb2'
  # @@protoc_insertion_point(class_scope:gitaly.AddNamespaceRequest)
  ))
_sym_db.RegisterMessage(AddNamespaceRequest)

RemoveNamespaceRequest = _reflection.GeneratedProtocolMessageType('RemoveNamespaceRequest', (_message.Message,), dict(
  DESCRIPTOR = _REMOVENAMESPACEREQUEST,
  __module__ = 'namespace_pb2'
  # @@protoc_insertion_point(class_scope:gitaly.RemoveNamespaceRequest)
  ))
_sym_db.RegisterMessage(RemoveNamespaceRequest)

RenameNamespaceRequest = _reflection.GeneratedProtocolMessageType('RenameNamespaceRequest', (_message.Message,), dict(
  DESCRIPTOR = _RENAMENAMESPACEREQUEST,
  __module__ = 'namespace_pb2'
  # @@protoc_insertion_point(class_scope:gitaly.RenameNamespaceRequest)
  ))
_sym_db.RegisterMessage(RenameNamespaceRequest)

NamespaceExistsRequest = _reflection.GeneratedProtocolMessageType('NamespaceExistsRequest', (_message.Message,), dict(
  DESCRIPTOR = _NAMESPACEEXISTSREQUEST,
  __module__ = 'namespace_pb2'
  # @@protoc_insertion_point(class_scope:gitaly.NamespaceExistsRequest)
  ))
_sym_db.RegisterMessage(NamespaceExistsRequest)

NamespaceExistsResponse = _reflection.GeneratedProtocolMessageType('NamespaceExistsResponse', (_message.Message,), dict(
  DESCRIPTOR = _NAMESPACEEXISTSRESPONSE,
  __module__ = 'namespace_pb2'
  # @@protoc_insertion_point(class_scope:gitaly.NamespaceExistsResponse)
  ))
_sym_db.RegisterMessage(NamespaceExistsResponse)

AddNamespaceResponse = _reflection.GeneratedProtocolMessageType('AddNamespaceResponse', (_message.Message,), dict(
  DESCRIPTOR = _ADDNAMESPACERESPONSE,
  __module__ = 'namespace_pb2'
  # @@protoc_insertion_point(class_scope:gitaly.AddNamespaceResponse)
  ))
_sym_db.RegisterMessage(AddNamespaceResponse)

RemoveNamespaceResponse = _reflection.GeneratedProtocolMessageType('RemoveNamespaceResponse', (_message.Message,), dict(
  DESCRIPTOR = _REMOVENAMESPACERESPONSE,
  __module__ = 'namespace_pb2'
  # @@protoc_insertion_point(class_scope:gitaly.RemoveNamespaceResponse)
  ))
_sym_db.RegisterMessage(RemoveNamespaceResponse)

RenameNamespaceResponse = _reflection.GeneratedProtocolMessageType('RenameNamespaceResponse', (_message.Message,), dict(
  DESCRIPTOR = _RENAMENAMESPACERESPONSE,
  __module__ = 'namespace_pb2'
  # @@protoc_insertion_point(class_scope:gitaly.RenameNamespaceResponse)
  ))
_sym_db.RegisterMessage(RenameNamespaceResponse)


DESCRIPTOR.has_options = True
DESCRIPTOR._options = _descriptor._ParseOptions(descriptor_pb2.FileOptions(), _b('Z2gitlab.com/gitlab-org/gitaly/v15/proto/go/gitalypb'))
_ADDNAMESPACEREQUEST.fields_by_name['storage_name'].has_options = True
_ADDNAMESPACEREQUEST.fields_by_name['storage_name']._options = _descriptor._ParseOptions(descriptor_pb2.FieldOptions(), _b('\210\306,\001'))
_REMOVENAMESPACEREQUEST.fields_by_name['storage_name'].has_options = True
_REMOVENAMESPACEREQUEST.fields_by_name['storage_name']._options = _descriptor._ParseOptions(descriptor_pb2.FieldOptions(), _b('\210\306,\001'))
_RENAMENAMESPACEREQUEST.fields_by_name['storage_name'].has_options = True
_RENAMENAMESPACEREQUEST.fields_by_name['storage_name']._options = _descriptor._ParseOptions(descriptor_pb2.FieldOptions(), _b('\210\306,\001'))
_NAMESPACEEXISTSREQUEST.fields_by_name['storage_name'].has_options = True
_NAMESPACEEXISTSREQUEST.fields_by_name['storage_name']._options = _descriptor._ParseOptions(descriptor_pb2.FieldOptions(), _b('\210\306,\001'))

_NAMESPACESERVICE = _descriptor.ServiceDescriptor(
  name='NamespaceService',
  full_name='gitaly.NamespaceService',
  file=DESCRIPTOR,
  index=0,
  options=None,
  serialized_start=442,
  serialized_end=827,
  methods=[
  _descriptor.MethodDescriptor(
    name='AddNamespace',
    full_name='gitaly.NamespaceService.AddNamespace',
    index=0,
    containing_service=None,
    input_type=_ADDNAMESPACEREQUEST,
    output_type=_ADDNAMESPACERESPONSE,
    options=_descriptor._ParseOptions(descriptor_pb2.MethodOptions(), _b('\372\227(\004\010\001\020\002')),
  ),
  _descriptor.MethodDescriptor(
    name='RemoveNamespace',
    full_name='gitaly.NamespaceService.RemoveNamespace',
    index=1,
    containing_service=None,
    input_type=_REMOVENAMESPACEREQUEST,
    output_type=_REMOVENAMESPACERESPONSE,
    options=_descriptor._ParseOptions(descriptor_pb2.MethodOptions(), _b('\372\227(\004\010\001\020\002')),
  ),
  _descriptor.MethodDescriptor(
    name='RenameNamespace',
    full_name='gitaly.NamespaceService.RenameNamespace',
    index=2,
    containing_service=None,
    input_type=_RENAMENAMESPACEREQUEST,
    output_type=_RENAMENAMESPACERESPONSE,
    options=_descriptor._ParseOptions(descriptor_pb2.MethodOptions(), _b('\372\227(\004\010\001\020\002')),
  ),
  _descriptor.MethodDescriptor(
    name='NamespaceExists',
    full_name='gitaly.NamespaceService.NamespaceExists',
    index=3,
    containing_service=None,
    input_type=_NAMESPACEEXISTSREQUEST,
    output_type=_NAMESPACEEXISTSRESPONSE,
    options=_descriptor._ParseOptions(descriptor_pb2.MethodOptions(), _b('\372\227(\004\010\002\020\002')),
  ),
])
_sym_db.RegisterServiceDescriptor(_NAMESPACESERVICE)

DESCRIPTOR.services_by_name['NamespaceService'] = _NAMESPACESERVICE

# @@protoc_insertion_point(module_scope)
